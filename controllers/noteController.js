const Note = require('../models/noteModel');


const getUserNotes = async (req, res) => {
  try {
    const userId = req.userId;
    const {offset = 0, limit = 0} = req.query;
    if (!userId) {
      return res.status(400).json({message: 'User is not found'});
    }
    const notes = await Note.find({userId}).skip(offset).limit(limit);
    const count = await Note.count({userId: userId});
    return res.status(200).json({
      offset: +offset,
      limit: +limit,
      count,
      notes,
    });
  } catch (error) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const addNote = async (req, res) => {
  try {
    const userId = req.userId;
    const text = req.body.text;
    if (!userId) {
      return res.status(400).json({message: 'User is not found'});
    }
    if (!text) {
      return res.status(400).json({message: 'Text is not specified'});
    }
    await Note.create({userId, text});
    return res.status(200).json({message: 'Note is created successfully'});
  } catch (error) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const getNoteById = async (req, res) => {
  try {
    const userId = req.userId;
    const noteId = req.params['id'];
    if (!userId) {
      return res.status(400).json({message: 'User is not found'});
    }
    if (!noteId) {
      return res.status(400).json({message: 'Note id is not specified'});
    }
    const note = await Note.findOne({_id: noteId, userId});
    if (!note) {
      return res.status(400).json({message: 'Note is not found'});
    }
    return res.status(200).json({note});
  } catch (error) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const updateNoteById = async (req, res) => {
  try {
    const noteId = req.params['id'];
    const userId = req.userId;
    const text = req.body.text;
    if (!userId) {
      return res.status(400).json({message: 'User is not found'});
    }
    if (!noteId) {
      return res.status(400).json({message: 'Note id is not specified'});
    }
    if (!text) {
      return res.status(400).json({message: 'Text is not specified'});
    }
    const note = await Note.findOne({_id: noteId, userId});
    if (!note) {
      return res.status(400).json({message: 'Note is not found'});
    }
    await Note.updateOne({_id: noteId, userId}, {text});
    return res.status(200).json({message: 'Note is updated successfully'});
  } catch (error) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const changeNoteCompleteStatus = async (req, res) => {
  try {
    const userId = req.userId;
    const noteId = req.params['id'];
    if (!userId) {
      return res.status(400).json({message: 'User is not found'});
    }
    if (!noteId) {
      res.status(400).json({message: 'Note id is not specified'});
      return;
    }
    const note = await Note.findOne({_id: noteId, userId});
    if (!note) {
      res.status(400).json({message: 'Note is not found'});
      return;
    }
    const result = await Note.updateOne({
      _id: noteId,
      userId,
    },
    {
      completed: !note.completed,
    });
    if (!result.acknowledged) {
      return res.status(500).json({message: 'Internal server error'});
    }
    return res.status(200)
        .json({message: 'Note complete status is changed successfully'});
  } catch (error) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const deleteNoteById = async (req, res) => {
  try {
    const userId = req.userId;
    const noteId = req.params['id'];
    if (!userId) {
      return res.status(400).json({message: 'User is not found'});
    }
    if (!noteId) {
      return res.status(400).json({message: 'Note id is not specified'});
    }
    const note = await Note.findOne({_id: noteId, userId});
    if (!note) {
      return res.status(400).json({message: 'Note is not found'});
    }
    const result = await Note.deleteOne({_id: noteId, userId});
    if (!result.acknowledged) {
      return res.status(500).json({message: 'Internal server error'});
    }
    return res.status(200).json({message: 'Note is deleted successfully'});
  } catch (error) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

module.exports = {
  getUserNotes, addNote,
  getNoteById, updateNoteById,
  changeNoteCompleteStatus, deleteNoteById,
};

