const User = require('../models/userModel');
const {validationResult} = require('express-validator');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const JWT_SECRET = process.env.JWT_SECRET;

const register = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({message: 'Registration error', errors});
    }

    const {username, password} = req.body;
    const foundUser = await User.findOne({username});

    if (foundUser) {
      return res.status(400).json({message: `Username is already taken`});
    }

    const hashPassword = bcrypt.hashSync(password, 7);
    await User.create({username, password: hashPassword});
    return res.status(200).json({message: 'User is registered successfully'});
  } catch (e) {
    res.status(500).json({message: 'Internal server error'});
  }
};


const login = async (req, res) => {
  try {
    const {username, password} = req.body;
    const foundUser = await User.findOne({username});
    if (!foundUser) {
      return res.status(400)
          .json({message: 'Username or password does not match'});
    }
    const validPassword = await bcrypt.compare(password, foundUser.password);
    if (!validPassword) {
      return res.status(400)
          .json({message: 'Username or password does not match'});
    }
    const token = jwt.sign({
      userId: foundUser._id},
    JWT_SECRET,
    {expiresIn: '1h'});

    return res.status(200)
        .json({message: 'Login is succesful', jwt_token: token});
  } catch (e) {
    res.status(500).json({message: 'Internal server error', e});
  }
};


module.exports = {
  register, login,
};
