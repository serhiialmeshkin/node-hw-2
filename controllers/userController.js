const User = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserInfo = async (req, res) => {
  try {
    const userId = req.userId;
    const foundUser = await User.findById(userId);

    if (!foundUser) {
      return res.status(400).json({message: 'User does not found'});
    }

    const {_id, username, createdDate} = foundUser;
    return res.status(200).json({user: {_id, username, createdDate}});
  } catch (error) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const deleteUserProfile = async (req, res) => {
  try {
    const userId = req.userId;
    const foundUser = await User.findById(userId);

    if (!foundUser) {
      return res.status(400).json( {message: 'User is not found'} );
    }

    await User.deleteOne(foundUser);
    return res.status(200).json( {message: 'User is deleted'} );
  } catch (error) {
    return res.status(500).json({message: 'Internal server error'});
  }
};

const changePassword = async (req, res) => {
  try {
    const userId = req.userId;
    const {oldPassword, newPassword} = req.body;

    if (!oldPassword) {
      return res.status(400)
          .json( {message: 'Old password should be specified'} );
    }

    if (!newPassword) {
      return res.status(400)
          .json( {message: 'New password should be specified'} );
    }

    const foundUser = await User.findById(userId);

    if (!foundUser) {
      return res.status(400).json({message: 'User is not found'});
    }

    const validPassword = await bcrypt.compare(oldPassword, foundUser.password);

    if (!validPassword) {
      return res.status(400).json({message: 'Old password is not valid'});
    }

    const hashPassword = await bcrypt.hash(newPassword, 7);
    await User.updateOne(foundUser, {password: hashPassword});

    return res.status(200).json({message: 'Password is updated'});
  } catch (error) {
    return res.status(500).json( {message: 'Internal server error'} );
  }
};

module.exports = {
  getUserInfo,
  deleteUserProfile,
  changePassword,
};
