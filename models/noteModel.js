const {Schema, model} = require('mongoose');

const note = new Schema({
  userId: {
    type: String,
    required: true,
  },

  completed: {
    type: Boolean,
    default: false,
  },

  text: {
    type: String,
    required: true,
  },

  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = model('Note', note);
