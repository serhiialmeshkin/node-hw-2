require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const apiRouter = require('./routers/index');
const PORT = process.env.PORT || 8080;
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors());
app.use('/api', apiRouter);

const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_DB_URL);
    app.listen(PORT, () => console.log(`server started on port ${PORT}`));
  } catch (e) {
    console.log(e);
  }
};

start();

module.exports = app;
