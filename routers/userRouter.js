const express = require('express');
const router = new express.Router();
const userController = require('../controllers/userController');


router.get('/', userController.getUserInfo);

router.delete('/', userController.deleteUserProfile);

router.patch('/', userController.changePassword);

module.exports = router;
