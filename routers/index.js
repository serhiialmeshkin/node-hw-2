const express = require('express');
const authRouter = require('./authRouter');
const authMiddleware = require('../middleware/authMiddleware');
const userRouter = require('./userRouter');
const noteRouter = require('./noteRouter');
const router = new express.Router();

router.use('/auth', authRouter);
router.use('/users/me', authMiddleware, userRouter);
router.use('/notes', authMiddleware, noteRouter);

module.exports = router;
