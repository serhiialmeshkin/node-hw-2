const express = require('express');
const router = new express.Router();
const noteController = require('../controllers/noteController');


router.get('/', noteController.getUserNotes);
router.post('/', noteController.addNote);
router.get('/:id', noteController.getNoteById);
router.put('/:id', noteController.updateNoteById);
router.patch('/:id', noteController.changeNoteCompleteStatus);
router.delete('/:id', noteController.deleteNoteById);

module.exports = router;
