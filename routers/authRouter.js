const express = require('express');
const router = new express.Router();
const authController = require('../controllers/authController');
const {check} = require('express-validator');

router.post('/register',
    check('username', 'Username should be not empty').notEmpty(),
    check('password', 'Password should be between 4 and 16 characters long')
        .isLength({min: 4, max: 16}),
    authController.register);
router.post('/login', authController.login);


module.exports = router;
